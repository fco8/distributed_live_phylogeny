# %% libraries
	import numpy as np
	import pandas as pd
	import os
	from math import inf
	import matplotlib.pyplot as plt
	import re
	from Bio import Phylo
	import io


# %% funciones

def s_score(i, j, m):
    if i == j:
        raise ValueError(
                'You cannot use the same index! Madness lies this way')
    n = m.shape[0]
    t1 = 1 / (2 * (n - 2))
    t2 = np.sum(np.add(
            m.iloc[np.delete(np.arange(n), j), i].values,
            m.iloc[np.delete(np.arange(n), i), j].values
            )
    )
    t3 = m.iloc[i, j] / 2
    t4 = 1 / (n - 2)
    c1 = m.columns[i]
    c2 = m.columns[j]
    m1 = m.drop([c1, c2])
    m1 = m.drop([c1, c2], axis=1)
    t5 = np.sum(np.tril(m1))
    return(t1 * t2 + t3 + t4 * t5)


def l_branches(i, j, m):
    n = m.shape[0]
    dij = m.iloc[i, j]
    dijj = np.sum(m.iloc[np.delete(np.arange(n), j), i].values) / (n - 2)
    djji = np.sum(m.iloc[np.delete(np.arange(n), i), j].values) / (n - 2)
    lix = (dij + dijj - djji) / 2
    ljx = (dij + djji - dijj) / 2
    return([lix, ljx])


def last_branch(i, j, k, m):
    lix = (m.iloc[i, j] + m.iloc[i, k] - m.iloc[j, k]) / 2
    ljx = (m.iloc[i, j] + m.iloc[j, k] - m.iloc[i, k]) / 2
    lkx = (m.iloc[i, k] + m.iloc[j, k] - m.iloc[i, j]) / 2
    return(np.abs([lix, ljx, lkx]))


def t_score(i, j, k, m):
    n = m.shape[0]
    c1 = m.columns[i]
    c2 = m.columns[j]
    m1 = m.drop([c1, c2])
    m1 = m.drop([c1, c2], axis=1)
    t = np.sum(np.tril(m1))
    return(m.iloc[i, k] + m.iloc[j, k] + t/(n-3))


def distancia_x(i, j, m):
    n = m.shape[0]
    r = np.empty([0])
    for y in np.delete(np.arange(n), [i, j]):
        r = np.append(r, (m.iloc[i, y] + m.iloc[j, y] - m.iloc[i, j]) / 2)
    r = np.append(r, 0)
    return(r)


def buscar_menor_s(m):
    n = m.shape[0]
    r = np.full([n, n], inf)
    for i in np.arange(n):
        for j in np.delete(np.arange(n), i):
            r[i, j] = s_score(i, j, m)
    pos = np.where(r == r.min())[0]
    return([r.min(), pos[0], pos[1]])


def buscar_menor_t(m):
    n = m.shape[0]
    r = np.full([n, n, n], inf)
    for u in np.arange(n):
        for v in np.delete(np.arange(n), u):
            for w in np.delete(np.arange(n), [u, v]):
                r[u, v, w] = t_score(u, v, w, m)
    pos = np.where(r == r.min())
    return([r.min(), pos[0][0], pos[1][0], pos[2][0]])


def crear_normal_edge(i, j, m, lista, s):
    d = l_branches(i, j, m)
    lista.append({'n1': m.index[i], 'n2': s, 'distancia': d[0]})
    lista.append({'n1': m.index[j], 'n2': s, 'distancia': d[1]})
    return(lista)


def crear_live_edge(u, v, w, m, lista):
    lista.append({'n1': m.index[u], 'n2': m.index[w],
                  'distancia': m.iloc[u, w]})
    lista.append({'n1': m.index[v], 'n2': m.index[w],
                  'distancia': m.iloc[v, w]})
    return(lista)


def crear_raiz_tres(m, lista, s):
    d = last_branch(0, 1, 2, m)
    lista.append({'n1': m.index[0], 'n2': s, 'distancia': d[0]})
    lista.append({'n1': m.index[1], 'n2': s, 'distancia': d[1]})
    lista.append({'n1': m.index[2], 'n2': s, 'distancia': d[2]})
    return(lista)


def crear_raiz_dos(m, lista):
    lista.append({'n1': m.index[0], 'n2': m.index[1],
                  'distancia': m.iloc[0, 1]})
    return(lista)


def conv_newick(df):
    # hacemos el arbol
    origen = df.n2.tail(1).values[0]
    raiz = df[df.n2 == origen]
    if raiz.shape[0] == 3:
        newick = '(' + str(raiz.n1.iloc[0]) + ':' + \
                 str(raiz.distancia.iloc[0]) + ',' + \
                 str(raiz.n1.iloc[1]) + ':' + str(raiz.distancia.iloc[1]) + \
                 ',' + str(raiz.n1.iloc[2]) + ':' + \
                 str(raiz.distancia.iloc[2]) + ')' + str(origen) + ';'
    else:
        newick = '(' + str(raiz.n1.iloc[0]) + ':' + \
                 str(raiz.distancia.iloc[0]) + ',' + str(raiz.n1.iloc[1]) + \
                 ':' + str(raiz.distancia.iloc[1]) + ')' + str(origen) + ';'
    df = df[df.n2 != origen]
    while df.shape[0] > 1:
        sig_nodo = df.n2.tail(1).values[0]
        bloque = df[df.n2 == sig_nodo]
        b_insert = '(' + bloque.n1.iloc[0] + ':' + \
                   str(bloque.distancia.iloc[0]) + ',' + bloque.n1.iloc[1] + \
                   ':' + str(bloque.distancia.iloc[0]) + ')'
        index = newick.find(sig_nodo)
        new_newick = newick[:index] + b_insert + newick[index:]
        newick = new_newick
        df = df[df.n2 != sig_nodo]
    return(re.sub('var[0-9]*', '', newick))


def crear_arbol(mat, debug=False):
    m = mat.copy()
    varc = 0
    lista = []
    while m.shape[0] > 3:
        if debug is True:
            print('Tamaño de matriz ' + str(m.shape[0]))
        min_s = buscar_menor_s(m)
        min_t = buscar_menor_t(m)
        if min_s[0] < min_t[0]:
            index_i = m.index[min_s[1]]
            index_j = m.index[min_s[2]]
            vector = distancia_x(min_s[1], min_s[2], m)
            s = 'var' + str(varc)
            varc += 1
            lista = crear_normal_edge(min_s[1], min_s[2], m, lista, s)
            m.drop(index_i, axis=0, inplace=True)
            m.drop(index_i, axis=1, inplace=True)
            m.drop(index_j, axis=0, inplace=True)
            m.drop(index_j, axis=1, inplace=True)
            m.loc[:, s] = 0
            m.loc[s] = vector
            m.loc[:, s] = vector
        else:
            index_u = m.index[min_t[1]]
            index_v = m.index[min_t[2]]
            lista = crear_live_edge(min_t[1], min_t[2], min_t[3], m, lista)
            m.drop(index_u, axis=0, inplace=True)
            m.drop(index_u, axis=1, inplace=True)
            m.drop(index_v, axis=0, inplace=True)
            m.drop(index_v, axis=1, inplace=True)
    if m.shape[0] == 3:
        s = 'var' + str(varc)
        varc += 1
        lista = crear_raiz_tres(m, lista, s)
    elif m.shape[0] == 2:
        lista = crear_raiz_dos(m, lista)
    return(conv_newick(pd.DataFrame(lista)))


def leer_matriz(ubicacion):
    matriz = pd.read_csv(ubicacion,
                         sep=' ',
                         skiprows=1,
                         header=None,
                         index_col=0)
    matriz.columns = matriz.index
    return(matriz)


def graficar_arbol(arbol, tamano=[16, 6]):
    plt.rcParams["figure.figsize"] = tamano
    Phylo.draw(Phylo.read(io.StringIO(arbol), "newick"))
    plt.show()



